const btn = document.getElementById("calculate");
const bill = document.getElementById("bill");
const tip = document.getElementById("tip");
const total = document.getElementById("total");

btn.addEventListener("click", calculateTotal);

function calculateTotal() {
  let billValue = parseFloat(bill.value);
  let tipValue = parseFloat(tip.value);
  let totalValue = billValue * ( 1 + tipValue / 100);
  total.innerHTML = `$${totalValue.toFixed(2)}`;
}
